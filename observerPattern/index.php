<!DOCTYPE html>
<html lang="fr" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title></title>
    </head>
    <body>
        <h1>Design pattern : Observer via PHP</h1>
        <h2>Description:</h2>
        <p>
            L'objectif de ce design pattern est de fournir à l'utilisateur un moyen d'ecouter des changement au sein d'une class
            et d'effectuer une action suite à ce changement.
            On peut mettre en place différente action en fonction de ce que produits le changement ( regarder le design pattern strategy).
            Lors de l'écoute, on peut donc ré-orienter un utilisateur via un callback (exemple: redirection, message d'erreur,etc...).

            La mise en place de ce design pattern ce base sur 2 interface /trait:
            <ul>
                <li>Subject</li>
                <li>Observer</li>
            </ul>
            Le " Subject " correspond à l'element qui serat mis en écoute par " observer " .
             " Observer "  quand à lui vas s'occuper d'effectuer les opérations lié à des changement sur le " Subject ".$
        </p>
    </body>
</html>
