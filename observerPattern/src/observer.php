<?php namespace App;


//Publisher
interface Subject{
    Public function attach($observer);
    Public function detach($index);
    Public function notify();
};

//Subscriber
interface Observer{
    public function handle();
};

class Login implements Subject {
    Protected $observers = [];

    Private function attachObserver($observable){
        foreach ($observable as $observer) {
            if(! $observer instanceof Observer){
                throw new Exception('Not a observer');
            }
            $this->attach($observer);
        }
    }

    Public function attach($observable){
        if(is_array($observable)){
            return $this->attachObserver($observable);
        }
        $this->observers[] = $observable;
        return $this;
    }

    Public function detach($index){
        unset($this->observers[$index]);
    }

    Public function notify(){
        foreach ($this->observers as $observer) {
            $observer->handle();
        }
    }

    Public function fire(){
        $this->notify();
    }

}

class LogHandler implements Observer{
    public function handle(){
        var_dump('log something');
    }
}

class EmailNotifier implements Observer{
    public function handle(){
        var_dump('send Email');
    }
}

class LoginReporter implements Observer{
    public function handle(){
        var_dump('log something about login');
    }
}
$login = new Login;
$login->attach(new LoginReporter);
$login->attach([new LogHandler,new EmailNotifier]);
$login->fire();
