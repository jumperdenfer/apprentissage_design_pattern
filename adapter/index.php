<?php
/**
* Adapter Pattern
* Description du site :
* An adapter is one of the easier design patterns to learn.
* The reason why is because you use them in the real world all the time! In this lesson,
* let's review a handful of examples to figure out how it all works.
*
* Compréhension personnel du design pattern adapter:
* le principe est de pouvoir " adapté " une class utilisant une autre interface afin de pouvoir l'utiliser avec l'interface voulue.
* Afin de réussir se procédé on vas donc créé une class inmplementant l'interface voulue, et prenant en paramètre la class d'origine( avec sont type)
* La class vas donc utiliser les méthodes de l'interface afin de faire la " transformation"
* Exmple présent dans src/eReaderAdapter
*
*/
require 'vendor/autoload.php';

use Acme\Book;
use Acme\BookInterface;
use Acme\eReaderInterface;
use Acme\Nook;
use Acme\Kindle;
use Acme\eReaderAdapter;


class Person{
    Public function read(BookInterface $book){
        $book->open();
        $book->turnPage();
    }
}
(new Person)->read(new eReaderAdapter(new Nook));
