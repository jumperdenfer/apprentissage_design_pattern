<?php namespace Acme;

class eReaderAdapter implements BookInterface{
    Private $reader;
    public function __construct(eReaderInterface $reader)
    {
        $this->reader = $reader;
    }
    public function open(){
        return $this->reader->turnOn();
    }
    Public function turnPage(){
        return $this->reader->pressNextButton();
    }
}
