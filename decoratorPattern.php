<?php
/**
* Principe du decorator Pattern suite à ma compréhension personnel
* Le decorator permet d'avoir un système d'heritage par encapsulation de class entre elle.
* Tout ces class implémente la même interface, seul la class d'origine ne dispose pas de constructeur.
* Chaque class encapsulable (hors celle de base) dispose d'un constructeur prenant la class précendant comme paramètre
* et la plaçant dans la class sous forme de variable via l'interface.
* L'objectif est donc de réduire la taille de class afin d'améliorer la lisibilité tout en permettant une réduction des bugs existante
*/

interface CarService
{
    public function getCost();
    Public function getDescription();
}

class BasicInspection implements CarService{
    Public function getCost(){
        return 19;
    }
    public function getDescription(){
        return 'basic inspection';
    }

}

class OilChange implements CarService{
    protected $carService;
    public function __construct(CarService $carService)
    {
        $this->carService = $carService;
    }
    public function getCost(){
        return 19 + $this->carService->getCost();
    }
    public function getDescription(){
        return $this->carService->getDescription() . ', and a oil change';
    }
}

class TireRotation implements CarService{
    protected $carService;
    public function __construct(CarService $carService)
    {
        $this->carService = $carService;
    }
    public function getCost(){
        return 19 + $this->carService->getCost();
    }
    public function getDescription(){
        return $this->carService->getDescription() . ', and a tire rotation';
    }
}
echo "basic inspection: ";
echo (new BasicInspection())->getCost();
echo '<br>';
echo "oil change: ";
echo (new OilChange(new BasicInspection()))->getCost();
echo "<br>";
echo "tire inspection: ";
echo (new TireRotation(new OilChange(new BasicInspection)))->getCost();
echo "<br>";
echo "description basic inspection: ";
$service = new BasicInspection;
echo $service->getDescription();
echo "<br>";
echo "description basic + tire: ";
$tire = new TireRotation(new BasicInspection);
echo $tire->getDescription();
