<?php namespace Controller;


//Ajoute les controller
use Controller\AccueilController;

class MainController{
    Protected $page;
    Protected $param;
    public function __construct($page,$param)
    {
        //Creation des routes
        if($page == 'getMethods'){

        }
        if($page == "accueil" || $page == ''){
            $page =  $this->accueilController();
        }

        if($page == "legume"){
            //Route avec un paramètre spécifique
            $this->param = $param;

            $page = $this->legumeController();
        }
    }
    //Création des fonction correspondante au route afin de charger les controller correspondant
    Public function accueilController(){
        return (new AccueilController)->generatePage();
    }
    Public function legumeController(){
        return (new LegumeController)->generatePage($this->param);
    }
}
