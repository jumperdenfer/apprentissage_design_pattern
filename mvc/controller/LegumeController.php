<?php namespace Controller;
use View\LegumeView;
use Model\ProduitModel;

class LegumeController{
    public function generatePage($param = null){

        if(! empty($param)){
            //Liste des paramètres accepté
            if( ! empty($param['nomLegume']) && ! empty($param['JSON'])){ //Affichage d'un légume spécifique au format JSON
                $legumeRaw = $param['nomLegume'];
                $legumeSelected['legume'] = (new ProduitModel)->getSpecifiqueLegume($legumeRaw);
                $legumeSelected['JSON'] = true;
                if($legumeSelected == null){
                    print_r('Aucun legume correspondant à été trouvé');
                    exit();
                }
                $view = (new LegumeView($legumeSelected));
                exit();
            }
            if(! empty($param['JSON'])){ //Affichage de tout les légumes en version JSON
                $legume['legume'] = (new ProduitModel)->getAllLegume();
                $legume['JSON'] = true;
                $view = new LegumeView($legume);
                exit();
            }
            if(! empty ($param['nomLegume'])){ //Affichage d'un legume spécifique en version web
                $legumeRaw = $param['nomLegume'];
                $legumeSelected = (new ProduitModel)->getSpecifiqueLegume($legumeRaw);
                if($legumeSelected == null){
                    print_r('Aucun legume correspondant à été trouvé');
                    exit();
                }
                $view = (new LegumeView($legumeSelected));
                exit();
            }
            if(! empty($param['page'])){//Affichage par défaut
                $legume = (new ProduitModel)->getAllLegume();
                $view = new LegumeView($legume);
                exit();
            }
        }
        else{
            print_r('Page Introuvable');
        }
    }
}
