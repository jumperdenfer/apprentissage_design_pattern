<?php namespace Controller;
use View\AccueilView;
use Model\ProduitModel;

class AccueilController{
    public function generatePage(){
        $view = (new AccueilView())->generateContent();
    }
}
