<?php namespace View;
class LegumeView{
    Private $data = '';
    public function __construct($data)
    {
        $this->data = $data;
        if(isset($this->data['JSON']) && $this->data['JSON']  == true){
            return $this->generateJsonContent();
        }
        if(is_array($this->data) && !isset($this->data['JSON'])){
            return $this->generateContent();
        }
        if(is_string($this->data) && !isset($this->data['JSON'])){
            return $this->generateSpecifiqueContent();
        }
    }

    Public function generateContent(){
        ob_start();
        ?>
            <h1>Liste des légumes disponible sur le marché:</h1>
            <ul>
        <?php
        foreach ($this->data as $element) {

            ?>
                <li>
                    <p><a href="index.php?page=legume&nomLegume=<?php echo $element ?>"> <?php echo $element ?></a></p>
                </li>
            <?php

        }
        ?>
            </ul>
        <?php
        $content = ob_get_clean();
        require('template.php');
    }
    Public function generateSpecifiqueContent(){
        ob_start();
        ?>
            <div>
                <h1>Notre meilleur légume du jour :</h1>
                <p><?php echo $this->data ?></p>
            </div>
        <?php
        $content = ob_get_clean();
        require('template.php');
    }
    Public function generateJsonContent(){
        header('Content-Type: application/json');
        echo json_encode($this->data['legume']);
    }
}
