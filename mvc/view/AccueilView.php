<?php namespace View;
class AccueilView{
    Public function generateContent(){
        //Permet d'écrire de l'html sans l'afficher
        ob_start();

            ?>
                <main>
                    <h1>Bienvenue sur la magnifique page d'accueil</h1>

                    <p>Ce site sert de mini-projet de réalisation d'un design pattern de type MVC</p>
                    <p>Aucune base de données n'est présentes, la plupart des données visible provienne de tableau statique</p>
                </main>
            <?php
        //Stock l'html précédement créé dans une variable, utilisé par template.php
        $content = ob_get_clean();
        require('template.php');
    }
}
