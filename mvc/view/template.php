<!DOCTYPE html>
<html lang="fr" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title></title>
    </head>
    <body>
        <header>
            <nav>
                <!-- Ajout des elements de navigation -->
                <?php echo $GLOBALS['nav'] ?>
            </nav>
        </header>
        <main>
            <!-- Ajout du contenue principale de la page -->
            <?php echo $content ?>
        </main>
    </body>
</html>
