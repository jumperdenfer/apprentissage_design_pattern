<?php
/**
* Attention, ce design pattern ne sert que d'exemple ! beaucoup de fonctionnalité " classique" sont manquante
* comme par exemple la vérification des données qui sont entrée via la méthode Get.
*
*/
require_once('vendor/autoload.php');
//Utilise le controller principale
use Controller\MainController;
//Utilise le RoadCatcher qui permet de générais la barre de navigation global
use Catcher\RoadCatcher;
//Créé la barre de navigation
$GLOBALS['nav'] = (new RoadCatcher(new MainController('getMethods','','')))->generateRoadHtml();
//Contient le nom de la page qui doit être afficher
$page = '';
//Contient les paramètres reçu en GET
$param = $_GET;
//Contient le contenue de la page (reçu depuis le controller)
$content ='';
//Si le paramètre page est passer
if(isset($param['page']) && $param['page'] != null){
    $page = (string) htmlspecialchars($param['page']);
}
//Fonction start qui sert à initialiser
function start($page,$param){
    $content = new MainController($page,$param);
}
//Initialisation
start($page,$param);
echo $content;


?>
