<?php namespace Catcher;
use Controller\MainController;
class RoadCatcher{
    Private $roads = [];
    Private $methodList;
    public function __construct($mainController){
        $this->methodList = get_class_methods($mainController);
        $this->addToRoad();
    }
    Public function addToRoad(){
        $methods = $this->methodList;
        foreach ($methods as $method) {
            if($method != '__construct'){
                $road = preg_split('/[A-Z]/',$method);
                array_push($this->roads,$road[0]);
            }
        }
    }
    Public function generateRoadHtml(){
        ob_start();
        foreach ($this->roads as $road) {
            ?>
            <!-- Ce Li à etait générais automatiquement -->
            <li><a href="index.php?page=<?php echo $road?>"><?php echo ucFirst($road)?></a></li>
            <?php
        }
        return $nav = ob_get_clean();
    }
}
