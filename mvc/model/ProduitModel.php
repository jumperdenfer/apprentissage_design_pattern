<?php namespace Model;
/**
* Class produit model
* Contient un tableau de données pour " emuler " la présence d'une base de données.
*/
class ProduitModel{
    Private $produits = [   'boisson' =>['coca','eau','orangina'],
                            'viande'=>['poulet','boeuf','porc'],
                            'legume'=>['endive','choux','salade','radis'],
                            'divers'=>['ampoule','pile','ecouteur','souris']
                        ];
    Public function getAllViande(){
        return $this->produits['viande'];
    }
    Public function getAllBoisson(){
        return $this->produits['boisson'];
    }
    Public function getAllLegume(){
        return $this->produits['legume'];
    }
    Public function getAllDivers(){
        return $this->produits['divers'];
    }
    Public function getSpecifiqueLegume($nomLegume){
        $legumeFind = array_search($nomLegume,$this->produits['legume']);
        if($legumeFind != null ||$legumeFind == 0){
            return $this->produits['legume'][$legumeFind];
        };
    }
}
