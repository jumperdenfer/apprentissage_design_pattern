<?php
/**
* Design pattern chain of responsibility
* Description personnel :
* Ce design pattern permet de créé une chaine ' virtuel' de class, chaque class nécessite
* que la précédente soit passer en OK avant d'effectuer ces propre action.
* Une class abstraite permet de définir les méthode de vérification et de à la class suivante
*
*/
abstract class HomeChecker{
    Protected $successor;
    public abstract function check(HomeStatus $home);
    public function succeedWith(HomeChecker $successor){
        $this->successor = $successor;
    }
    public function next(HomeStatus $home){
        if($this->successor){
            $this->successor->check($home);
        }
    }
}
class Locks extends HomeChecker{
    public function check(HomeStatus $home){
        if(! $home->locked){
            throw new Exception('The doors are not locked ! Abort abort.');
        }
        $this->next($home);
    }
}
class Lights extends HomeChecker{
    public function check(HomeStatus $home){
        if(! $home->lightsOff){
            throw new Exception('The light are still ON ! Abort abort.');
        }
        $this->next($home);
    }
}
class Alarm extends HomeChecker{
    public function check(HomeStatus $home){
        if(! $home->alarmOn){
            throw new Exception('The alarm has not been set ! Abort abort.');
        }
        $this->next($home);
    }
}
class HomeStatus{
    public $alarmOn = true;
    public $locked = true;
    public $lightsOff = true;
}
$locks = new Locks;
$alarm = new Alarm;
$lights = new Lights;
$locks->succeedWith($lights);
$lights->succeedWith($alarm);

$locks->check(new HomeStatus);
