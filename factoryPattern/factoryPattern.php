<?php
//Fichier de teste via le design pattern Factory
//Exemple avec une class d'accès à des base de données
interface Crud{
    public function create();
    public function read();
    Public function update();
    public function delete();
}
class DBFactory{
    public static function create($connectionString){
        //Vérifie que la requête est au bon format 'string:string'
        if (($driverEndPos = strpos ($connectionString, ':')) === false){
            throw new Exception ('Mauvaise chaine de connexion');
        }
        //Découpe la requête et récupère la première partie, qui doit correspondre au nom type de base de données
        switch (substr ($connectionString, 0, $driverEndPos)){
            case 'mysql':
                $db = new DBMySql ($connectionString);
                break;
            case 'oracle':
                $db = new DBOracle ($connectionString);
                break;
            default:
                throw new Exception ('Type de base inconnu');
        }
        return $db;
    }
}
class DBMySql implements Crud{
    public function __construct($connectionString){
        //Effectue la connection
    }
    public function create(){

    }
    public function read(){
        return 'Vous êtes bien dans mysql';
    }
    Public function update(){

    }
    public function delete(){

    }
}
class DBOracle implements Crud{
    public function __construct($connectionString){
        //Effectue la connection
    }
    public function create(){

    }
    public function read(){
        return 'Vous êtes bien dans mysql';
    }
    Public function update(){

    }
    public function delete(){

    }
}


//Connexion à une base de données de type mysql

$mysqlQuery = DBFactory::create('mysql://user.password@localhost');
var_dump($mysqlQuery->read());
