# apprentissage_design_pattern

Ce dépôt GIT contient mes expérience avec divers Design pattern,  la plupart des projets sont lié à des cours disponible sur laracast ou d'autre sites, sauf celui pour le MVC

## Partie MVC
Découpage:
- Controller
- Model
- View
- Catcher

Les 3 éléments du MVC classique sont donc bien présent. Un super controller ( qui sert de router) nommé mainController redirige en fonction des paramètres GET l'utilisateur vers la page spécifique ou l'accueil.

Le model, correspond à une connexion avec une base de données, aucune base n'est utilisé dans le projet.

Les view corresponde aux visuel des pages, elle sont générais en php via ob_start() et ob_get_clean()

Catcher, quand à lui contient des class trés spécifique et pas directement lié à ce design pattern,
on y retrouve notamment le RoadCatcher qui vas " catch" toute les fonctions présente dans le MainController et créé
une barre de navigation correspondant.
