<?php namespace App;
/**
* Design pattern Strategy
* Description personnel du design pattern:
*   L'objective et d'avoir plusieurs ' strategy ' pour traiter un flux d'information
*   Plusieur class sont créé à partir de la m^ême interface.
*   Lors de l'execution de la classe gérant les strategy, il suffit de passer en paramètre la class de strategy que l'on souhaite utiliser
*   si aucun paramètre est entrer, une strategy par défaut doit être mis en place
**/




//Encapsulate and make them interchangeable

interface Logger{

    Public function log($data);

}
//Define a family of algorythm
class LogToFile implements Logger{

    Public function log($data){
        var_dump('Log the data to a file');
    }
}
class LogToDatabase implements Logger{

    Public function log($data){
        var_dump('Log the data to the database');
    }

}
class LogToXWebService implements Logger{

    Public function log($data){
        var_dump('Log the data to a SaaS');
    }

}
class App{
    Public function log($data, Logger $logger = null){
        $logger = $logger ?: new LogToFile;
        $logger->log($data);
    }
}

$app = new App;
$app->log('some information', new LogToDatabase);
