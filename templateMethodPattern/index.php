<?php
/**
* Design Pattern Template Method
* Description personnel du pattern:
* Base sur les class abstract et les fonction abstract il permet d'éviter les " doublon " de fonction
* grâce aux fonction abstraite. Dans l'exmple ici, chaque class de sandiwch hérite de la class Sub,
* seul le type d'ingrédient primaire est changé dans les sous-class via la class abstraite.
*
*/
require 'vendor/autoload.php';

//Créé un sandwich
(new App\TurkeySub)->make();
//Créé un autre sandwich
(new App\VeggieSub)->make();
