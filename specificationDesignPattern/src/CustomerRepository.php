<?php namespace App;
class CustomerRepository{
    Protected $customers;
    public function __construct($customers)
    {
        $this->customers = $customers;
    }
    Public function all(){
        return $this->customers;
    }
    Public function matchingSpecification($specification){
        $matches = [];
        foreach ($this->customers as $customer) {
            if($specification->isSatisfiedBy($customer)){
                $matches[] = $customer;
            }

        }
        return $matches;
    }
}
