<?php
/**
* Design pattern specification
* Description personnel:
* Ce design pattern fait partie du Test Driven Developpement
* L'objectif de ce pattern est de test les fonctionnalité tout en les développant, en vérifiant à chaque fois qu'elle reussisse les test mis en place
* Ce design pattern nécessite forcement la présence d'une librairie de test , cas présent : " PHPUnit"
*
* Via ce design pattern, le developpement se retrouve allongé mais la correction du bug est immédiate, ce qui peut permettre au final de gagné du temps
*
*
*/
