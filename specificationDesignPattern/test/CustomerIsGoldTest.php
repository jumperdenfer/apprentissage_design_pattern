<?php namespace App\test;

use App\Customer;
use App\CustomerIsGold;
use PHPUnit\Framework\TestCase;

class CustomerIsGoldTest extends TestCase{
    /** test **/
    function testCustomerIsGold(){
        $specification = new CustomerIsGold;
        $goldCustomer = new Customer('gold');
        $silverCustomer = new Customer('silver');
        $specification->isSatisfiedBy($goldCustomer);
        $this->assertTrue($specification->isSatisfiedBy($goldCustomer));
        $this->assertFalse($specification->isSatisfiedBy($silverCustomer));
    }
}
