<?php namespace App\test;

use App\Customer;
use App\CustomerIsGold;
use App\CustomerRepository;
use PHPUnit\Framework\TestCase;

class CustomerRepositoryTest extends TestCase{
    Protected $customers;
    Public function setUp(){
        $this->customers = new CustomerRepository(
            [
                new Customer('silver'),
                new Customer('gold'),
                new Customer('silver'),
                new Customer('gold'),
                new Customer('bronze'),
                new Customer('silver')
            ]
        );
    }
    function testAllCustomer(){
        $results = $this->customers->all();
        $this->assertCount(6,$results);
    }


    function testRepositoryCustomer(){

        $results = $this->customers->matchingSpecification(new CustomerIsGold);
        $this->assertCount(2,$results);
    }
}
